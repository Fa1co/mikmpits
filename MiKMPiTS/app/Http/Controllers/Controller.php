<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function index(){
//        створюємо рядок не в області видимості класу Text
        $str = new TextString('Реалізація агрегації');
        {
//            створимо текст і передамо в нього вище створений рядок
            $txt = new Text($str);
//            виведемо вміст $txt
            var_dump($txt->getText());
            echo '<br>';
        } //$txt покидає зону видимості і знищується в той час як $str і надалі доступний

//        виведемо вміст $srt
        var_dump($str->getString());
    }
}
