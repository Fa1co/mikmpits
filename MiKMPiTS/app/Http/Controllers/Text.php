<?php
/**
 * Created by PhpStorm.
 * User: Sokol
 * Date: 02.11.2020
 * Time: 15:30
 */

namespace App\Http\Controllers;


class Text
{
    private $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function __destruct()
    {
        unset($this->text);
    }

    public function getText(){
        return $this->text;
    }

    public function clearText(){
        $this->text = '';
    }

    public function searchInText($search){
        return strpos($this->text,$search);
    }

}