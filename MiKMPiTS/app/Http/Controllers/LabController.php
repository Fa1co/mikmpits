<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LabController extends Controller
{
    public function lab1Index(){
        return view('lab1.index');
    }

    public function lab1Calc(Request $request){
        $N = $request->N;
        $H1 = $request->H1;
        $H2 = $request->H2;
        $l = $request->l;
        $D = $request->D;
        $h = $request->h;
        $sigma = $request->sigma;
        $t1 = $request->t1;
        $t2 = $request->t2;
        $t3 = $request->t3;
        $v1 = $request->v1;
        $a1 = $request->a1;
        $tao = $t2-$t1;

        $xAxis = [10,20,30,40,50,60,70,80,90,100];

        $V = (($H1 - $H2) / $l) * $D;
        $a = ($tao * ($D + $V * $h)) / ($sigma * $h*$h);
        $b = ($D * $h) / ($h*$h * $sigma);
        $c = (2*$D*$tao + $sigma * $h*$h + $V*$h*$tao) / ($sigma * $h*$h);
//        нульовий шар
        $cx0 = [];

        foreach ($xAxis as $item){
            $cx0[] = pow(sin($item * $N),2);
        }
        $c10 = $a1 * pow($t1,($v1 * $N)) * exp(-$v1*$t1);
        $c20 = $a1 * pow($t2,($v1 * $N)) * exp(-$v1*$t2);
        $c30 = $a1 * pow($t3,($v1 * $N)) * exp(-$v1*$t3);

        $c0 = $N - $b + (pow(sin($N*$l),2));
        $c1 = 0.2;
        $v = 0;
        $cLastFirstLevel = exp(-$v*$t1) * ($c0 * $t1 + $c1 * $t1);
        $cLastSecondLevel = exp(-$v*$t2) * ($c0 * $t2 + $c1 * $t2);
        $cLastThirdLevel = exp(-$v*$t3) * ($c0 * $t3 + $c1 * $t3);

//        для першого часового шару t = τ (к=1)
        $coefForFirstLevel = [['alpha'=>0, 'betta'=>$c10]];

        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForFirstLevel[] = [
                'alpha'=>$b / ($c - $coefForFirstLevel[$prevIndex]['alpha']* $a),
                'betta'=>($a*$coefForFirstLevel[$prevIndex]['betta'] + $cx0[$i]) / ($c - $coefForFirstLevel[$prevIndex]['alpha']* $a),
            ];
        }

        $c1x[count($xAxis) -1 ] = $cLastFirstLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $c1x[$i] = $c1x[$prevIndex] * $coefForFirstLevel[$i]['alpha'] + $coefForFirstLevel[$i]['betta'];
        }

//        для другого часового шару t = 2τ (к=2)
        $coefForSecondLevel = [['alpha'=>0, 'betta'=>$c20]];
        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForSecondLevel[] = [
                'alpha'=>$b / ($c - $coefForSecondLevel[$prevIndex]['alpha']* $a),
                'betta'=>($a*$coefForSecondLevel[$prevIndex]['betta'] + $c1x[$i]) / ($c - $coefForSecondLevel[$prevIndex]['alpha']* $a),
            ];
        }

        $c2x[count($xAxis) -1 ] = $cLastSecondLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $c2x[$i] = ($c2x[$prevIndex] * $coefForSecondLevel[$i]['alpha'] + $coefForSecondLevel[$i]['betta']);
        }

//           для третього часового шару t = 3τ (к=3)
        $coefForThirdLevel = [['alpha'=>0, 'betta'=>$c30]];
        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForThirdLevel[] = [
                'alpha'=>$b / ($c - $coefForThirdLevel[$prevIndex]['alpha']* $a),
                'betta'=>($a*$coefForThirdLevel[$prevIndex]['betta'] + $c2x[$i]) / ($c - $coefForThirdLevel[$prevIndex]['alpha']* $a),
            ];
        }

        $c3x[count($xAxis) -1 ] = $cLastThirdLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $c3x[$i] = ($c3x[$prevIndex] * $coefForThirdLevel[$i]['alpha'] + $coefForThirdLevel[$i]['betta']);
        }

        $c3x[0] = $c30;
        $c2x[0] = $c20;
        $c1x[0] = $c10;

        $chartjs = app()->chartjs
            ->name('lineChartTest')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($xAxis)
            ->datasets([
                [
                    "label" => "Нульовий рівень",
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($cx0),
                ],
                [
                    "label" => "Перший рівень",
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($c1x)),
                ],
                [
                    "label" => "Другий рівень",
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($c2x)),
                ],
                [
                    "label" => "Третій рівень",
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($c3x)),
                ]
            ])
            ->options([]);

        $result = [$cx0,array_reverse($c1x),array_reverse($c2x),array_reverse($c3x)];
        return view('lab1.chart', compact('chartjs','result'));
    }

    public function lab2Index(){
        return view('lab2.index');
    }

    public function lab2Calc(Request $request){
        $N = $request->N;
        $H1 = $request->H1;
        $H2 = $request->H2;
        $l = $request->l;
        $D = $request->D;
        $h = $request->h;
        $k = $request->k;
        $sigma = $request->sigma;
        $tao = $request->tao;
        $lambda = $request->lambda;

        $xAxis = [10,20,30,40,50,60,70,80,90,100];

        //        розподіл температур

        $T1 = 20 * $N;
        $T2 = 8 + $N;
        $Cp = 4200000;
        $Cn = 3000000;
        $v = ($k * ($H1 - $H2)) / $l;
        $u = 1/(1+(($h*$v)/(2*$lambda)));
        $rPlus = 0;
        $rMinus = - ($v*$Cp) / $lambda;
        $a = $Cp / pow($h,2) - $rMinus/$h;
        $b = $u/(pow($h,2)) + $rMinus/$h;
        $c = 2* $u / pow($h,2) + $rPlus/$h - $rMinus/$h + $Cn/($lambda*$tao);
        $ct10 = $T1;
        $ct20 = $T1;
        $ct30 = $T1;
        $ctLastFirstLevel = $T2;
        $ctLastSecondLevel = $T2;
        $ctLastThirdLevel = $T2;
//        нульовий шар
        $ctx0[] = $T1;

        for ($i = 1; $i < count($xAxis); $i++){
            $ctx0[] = (($T2 - $T1)*$i)/count($xAxis) + $T1;
        }
//        для першого часового шару t = τ (к=1)
        $coefForFirstLevel = [['alpha'=>0,'F'=>(($Cn / ($lambda * $tao)) * $ctx0[0]), 'betta'=>$ct10]];
        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForFirstLevel[] = [
                'alpha'=>$b / ($c - $coefForFirstLevel[$prevIndex]['alpha']* $a),
                'F'=>($Cn / ($lambda * $tao)) * $ctx0[$prevIndex],
                'betta'=>($a *$coefForFirstLevel[$prevIndex]['betta'] + $coefForFirstLevel[$prevIndex]['F'])/($c - $coefForFirstLevel[$prevIndex]['alpha']* $a) ,
            ];
        }

        $ct1x[count($xAxis) -1 ] = $ctLastFirstLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $ct1x[$i] = $ct1x[$prevIndex] * $coefForFirstLevel[$i]['alpha'] + $coefForFirstLevel[$i]['betta'];
        }
        $ct1x[0] = $ct10;
//        для другого часового шару t = 2τ (к=2)
        $coefForSecondLevel = [['alpha'=>0,'F'=>(($Cn / ($lambda * $tao)) * $ct1x[0]), 'betta'=>$ct20]];
        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForSecondLevel[] = [
                'alpha'=>$b / ($c - $coefForSecondLevel[$prevIndex]['alpha']* $a),
                'F'=>($Cn / ($lambda * $tao)) * $ct1x[$prevIndex],
                'betta'=>($a *$coefForSecondLevel[$prevIndex]['betta'] + $coefForSecondLevel[$prevIndex]['F'])/($c - $coefForSecondLevel[$prevIndex]['alpha']* $a) ,
            ];
        }

        $ct2x[count($xAxis) -1 ] = $ctLastSecondLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $ct2x[$i] = $ct2x[$prevIndex] * $coefForSecondLevel[$i]['alpha'] + $coefForSecondLevel[$i]['betta'];
        }
        $ct2x[0] = $ct20;

//        для третього часового шару t = 3τ (к=3)
        $coefForThirdLevel = [['alpha'=>0,'F'=>(($Cn / ($lambda * $tao)) * $ct2x[0]), 'betta'=>$ct30]];
        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForThirdLevel[] = [
                'alpha'=>$b / ($c - $coefForThirdLevel[$prevIndex]['alpha']* $a),
                'F'=>($Cn / ($lambda * $tao)) * $ct2x[$prevIndex],
                'betta'=>($a *$coefForThirdLevel[$prevIndex]['betta'] + $coefForThirdLevel[$prevIndex]['F'])/($c - $coefForThirdLevel[$prevIndex]['alpha']* $a) ,
            ];
        }

        $ct3x[count($xAxis) -1 ] = $ctLastThirdLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $ct3x[$i] = $ct3x[$prevIndex] * $coefForThirdLevel[$i]['alpha'] + $coefForThirdLevel[$i]['betta'];
        }
        $ct3x[0] = $ct30;

//        розподіл концентрацій

        $V = (($H1 - $H2) / $l) * $D;
        $a = ($tao * ($D + $V * $h)) / ($sigma * $h*$h);
        $b = ($D * $h) / ($h*$h * $sigma);
        $c = (2*$D*$tao + $sigma * $h*$h + $V*$h*$tao) / ($sigma * $h*$h);
//        нульовий шар
        $cx0 = [];

        foreach ($xAxis as $item){
            $cx0[] = 0;
//            $cx0[] = pow(1*$tao,2) * exp (-0.1 * 1*$tao * $N);
        }
//        $cx0[0] = 0;
//        $cx0[9] = 0;
        $c10 = pow(1*$tao,2) * exp (-0.1 * 1*$tao * $N);
//        $c10 = 0;
        $c20 = pow(2*$tao,2) * exp (-0.1 * 2*$tao * $N);
//        $c20 = 0;
        $c30 = pow(3*$tao,2) * exp (-0.1 * 3*$tao * $N);
//        $c30 = 0;
// @todo подставить температуру в розрахунок концентрації

        $cLastFirstLevel = pow(1*$tao,2) * exp (-0.2 * 1*$tao * $N);
        $cLastSecondLevel = pow(2*$tao,2) * exp (-0.2 * 2*$tao * $N);
        $cLastThirdLevel = pow(3*$tao,2) * exp (-0.3 * 3*$tao * $N);

//        для першого часового шару t = τ (к=1)
        $coefForFirstLevel = [['alpha'=>0, 'betta'=>$c10]];

        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForFirstLevel[] = [
                'alpha'=>$b / ($c - $coefForFirstLevel[$prevIndex]['alpha']* $a),
                'betta'=>($a*$coefForFirstLevel[$prevIndex]['betta'] + $ctx0[$i]) / ($c - $coefForFirstLevel[$prevIndex]['alpha']* $a),
            ];
        }

        $c1x[count($xAxis) -1 ] = $cLastFirstLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $c1x[$i] = $c1x[$prevIndex] * $coefForFirstLevel[$i]['alpha'] + $coefForFirstLevel[$i]['betta'];
        }

//        для другого часового шару t = 2τ (к=2)
        $coefForSecondLevel = [['alpha'=>0, 'betta'=>$c20]];
        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForSecondLevel[] = [
                'alpha'=>$b / ($c - $coefForSecondLevel[$prevIndex]['alpha']* $a),
                'betta'=>($a*$coefForSecondLevel[$prevIndex]['betta'] + $ct1x[$i]) / ($c - $coefForSecondLevel[$prevIndex]['alpha']* $a),
            ];
        }

        $c2x[count($xAxis) -1 ] = $cLastSecondLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $c2x[$i] = ($c2x[$prevIndex] * $coefForSecondLevel[$i]['alpha'] + $coefForSecondLevel[$i]['betta']);
        }

//        для третього часового шару t = 3τ (к=3)
        $coefForThirdLevel = [['alpha'=>0, 'betta'=>$c30]];
        for($i = 1; $i < count($xAxis); $i++){
            $prevIndex = $i - 1;
            $coefForThirdLevel[] = [
                'alpha'=>$b / ($c - $coefForThirdLevel[$prevIndex]['alpha']* $a),
                'betta'=>($a*$coefForThirdLevel[$prevIndex]['betta'] + $ct2x[$i]) / ($c - $coefForThirdLevel[$prevIndex]['alpha']* $a),
            ];
        }

        $c3x[count($xAxis) -1 ] = $cLastThirdLevel;
        for($i = count($xAxis) -2; $i > 0; $i--){
            $prevIndex = $i + 1;
            $c3x[$i] = ($c3x[$prevIndex] * $coefForThirdLevel[$i]['alpha'] + $coefForThirdLevel[$i]['betta']);
        }

        $c3x[0] = $c30;
        $c2x[0] = $c20;
        $c1x[0] = $c10;

        $chartjs = app()->chartjs
            ->name('Chart1')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($xAxis)
            ->datasets([
                [
                    "label" => "Нульовий рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($cx0),
                ],
                [
                    "label" => "Перший рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($c1x)),
                ],
                [
                    "label" => "Другий рівень",
                    'backgroundColor' => "rgba(98, 181, 31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($c2x)),
                ],
                [
                    "label" => "Третій рівень",
                    'backgroundColor' => "rgba(47, 161, 157)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($c3x)),
                ]
            ])
            ->options([]);



        $chartjs1 = app()->chartjs
            ->name('Chart2')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($xAxis)
            ->datasets([
                [
                    "label" => "Нульовий рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($ctx0),
                ],
                [
                    "label" => "Перший рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($ct1x)),
                ],
                [
                    "label" => "Другий рівень",
                    'backgroundColor' => "rgba(98, 181, 31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($ct2x)),
                ],
                [
                    "label" => "Третій рівень",
                    'backgroundColor' => "rgba(47, 161, 157)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values(array_reverse($ct3x)),
                ]
            ])
            ->options([]);
        $result = [$cx0,array_reverse($c1x),array_reverse($c2x),array_reverse($c3x)];
        $result_temp = [$ctx0,array_reverse($ct1x),array_reverse($ct2x),array_reverse($ct3x)];
        return view('lab2.chart', compact('chartjs','chartjs1','result','result_temp'));
    }



    public function lab3Index(){
        return view('lab3.index');
    }

    public function lab3Calc(Request $request){
        $N = $request->N;
        $D = $request->D;
        $h1 = $request->h1;
        $h2 = $request->h2;
        $sigma = $request->sigma;
        $DT = $request->DT;
        $tau = $request->tau;
        $y = $request->y;
        $l = $request->l;
        $b = $request->b;
        $Cm = 350;
        $C = $request->C * $Cm;
        $C1 = 350;
        $C2 = (int)$N;
        $H1=1.5;
        $H2=0.5;
        $k=1.5;

        $V=(($k*($H1-$H2))/$l);
        $u = 1/(1+(($h1*$V)/(2*$D)));
        $r = -($V/$D);
        $r_minus = 0.5*($r - abs($r));
        $r_plus = 0.5*($r + abs($r));
        $a1 = ($u/($h1 * $h1))-($r_minus/$h1);
        $b1 = ($u/($h1 * $h1))+($r_plus/$h1);
        $c1 = ((2*$u)/($h1 * $h1)) +($r_plus/$h1) - ($r_minus/$h1) + ($y/(2*$D)) + ($sigma/($D * $tau));
        $a2 = 1/($h2 * $h2);
        $b2 = 1/($h2 * $h2);
        $c2 = (2/($h2 * $h2)) + ($y/(2*$D)) + ($sigma/($D * $tau));

        $tmp_result = [];

        for ($i = 0; $i < 7; $i++){
            for ($j = 0; $j < 11; $j++){
                if($j === 0){
                    $tmp_result[1]['full'][$i][$j] = $C1;
                } elseif ($j === 10){
                    $tmp_result[1]['full'][$i][$j] = $C2;
                } else {
                    $tmp_result[1]['full'][$i][$j] = ((($C2 - $C1)/$l) * $j) + $C1;
                }
            }
        }
        $indexPrevLevel = 1;
        for ($l = 2; $l <=4 ; $l++) {
            for ($i = 0; $i < 7; $i++) {
                $index = 0;
                for ($j = 0; $j < 11; $j++) {
                    if ($j === 0) {
                        $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $j] = 0;
                        $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $j] = $C1;
                    } else {
                        $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $j] = $b1 / ($c1 - $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $index] * $a1);
                        $tmp_result[$indexPrevLevel]['half'][$i]['f-' . $j] = ($y/(2*$D)) * $C + ($sigma/($tau * $D))*$tmp_result[$indexPrevLevel]['full'][$i][$j];
                        $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $j] = ($a1 * $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $index] + $tmp_result[$indexPrevLevel]['half'][$i]['f-' . $j]) /($c1 - ($tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $index] * $a1));
                        $index++;
                    }

                }
            }

            for ($i = 0; $i < 7; $i++) {
                for ($j = 10; $j >= 0; $j--) {
                    if ($j === 0) {
                        $tmp_result[$l]['half'][$i][$j] = $C1;
                    } elseif ($j === 10) {
                        $tmp_result[$l]['half'][$i][$j] = $C2;
                    } else {
                        $tmp_result[$l]['half'][$i][$j] = $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $j] * $tmp_result[$indexPrevLevel]['full'][$i][$j] + $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $j];
                    }
                }
            }

            for ($i = 0; $i < 10; $i++) {
                $index = 0;
                for ($j = 0; $j < 7; $j++) {
                    if ($j === 0) {
                        $tmp_result[$l]['full'][$i]['alpha-' . $j] = 0;
                        $tmp_result[$l]['full'][$i]['betta-' . $j] = $C;
                    } else {
                        $tmp_result[$l]['full'][$i]['alpha-' . $j] = $b2 / ($c2 - $tmp_result[$l]['full'][$i]['alpha-' . $index] * $a2);
                        $tmp_result[$l]['full'][$i]['f-' . $j] = ($y/(2*$D)) * $C + ($sigma/($tau * $D))*$tmp_result[$l]['half'][$j][$i];
                        $tmp_result[$l]['full'][$i]['betta-' . $j] = ($a2 * $tmp_result[$l]['full'][$i]['betta-' . $index] + $tmp_result[$l]['full'][$i]['f-' . $j]) /($c2 - ($tmp_result[$l]['full'][$i]['alpha-' . $index] * $a2));
                        $index++;
                    }
                }
            }
            for ($i = 0; $i < 10; $i++) {
                for ($j = 6; $j >= 0; $j--) {
                    if ($j === 6) {
                        $tmp_result[$l]['full'][$i][$j] = $tmp_result[$l]['full'][$i]['alpha-' . $j] * ($tmp_result[$l]['full'][$i]['betta-' . $j]/(1-$tmp_result[$l]['full'][$i]['alpha-' . $j])) + $tmp_result[$l]['full'][$i]['betta-' . $j];
                    } else {
                        $tmp_result[$l]['full'][$i][$j] = $tmp_result[$l]['full'][$i]['alpha-' . $j] * $tmp_result[$indexPrevLevel]['full'][$j][$i] + $tmp_result[$l]['full'][$i]['betta-' . $j];
                    }
                }
            }
            for ($i = 0; $i < 7; $i++){
                for ($j = 0; $j < 11; $j++){
                    if($j === 0){
                        $tmp_result[$l]['full'][$i][$j] = $C1;
                    } elseif ($j === 10){
                        $tmp_result[$l]['full'][$i][$j] = $C2;
                    } else {
                        $tmp_result[$l]['full'][$i][$j] = $tmp_result[$l]['full'][$j][$i];
                    }
                }
            }
            $indexPrevLevel++;

        }


        for ($l = 1; $l <=4 ; $l++) {
            for ($i = 1; $i <= 6; $i++) {
                for ($j = 0; $j < 11; $j++) {
                    $result[$l][$i][$j] = $tmp_result[$l]['full'][$i][$j];
                }
            }
        }
//        dd($result[3]);
        return view('lab3.char',compact('result'));
    }

    public function lab4Index(){
        return view('lab4.index');
    }

    public function lab4Calc(Request $request){
        $N = $request->N;
        $D = $request->D;
        $h1 = $request->h1;
        $h2 = $request->h2;
        $sigma = $request->sigma;
        $DT = $request->DT;
        $tau = $request->tau;
        $y = $request->y;
        $l = $request->l;
        $b = $request->b;
        $T1 = $request->T1;
        $T2 = $request->T2;
        $T3 = $request->T3;
        $Cm = 350;
        $C = $request->C * $Cm;
        $C1 = 350;
        $C2 = (int)$N;
        $H1=1.5;
        $H2=0.5;
        $k=1.5;


        $V=(($k*($H1-$H2))/$l);
        $u = 1/(1+(($h1*$V)/(2*$D)));
        $r = -($V/$D);
        $r_minus = 0.5*($r - abs($r));
        $r_plus = 0.5*($r + abs($r));
        $a1 = ($u/($h1 * $h1))-($r_minus/$h1);
        $b1 = ($u/($h1 * $h1))+($r_plus/$h1);
        $c1 = ((2*$u)/($h1 * $h1)) +($r_plus/$h1) - ($r_minus/$h1) + ($y/(2*$D)) + ($sigma/($D * $tau));
        $a2 = 1/($h2 * $h2);
        $b2 = 1/($h2 * $h2);
        $c2 = (2/($h2 * $h2)) + ($y/(2*$D)) + ($sigma/($D * $tau));
        $tmp_result = [];
        /*температура*/
        $u1 = 1/(1+0.5*$h1*abs($r));
        $a21= $a1;
        $b21= $b1;
        $c21 = ((2*$u1)/($h1 * $h1)) +($r_plus/$h1) - ($r_minus/$h1) + ($h1/$tau);
        $a22 = 1/($h2 * $h2);
        $b22 = 1/($h1 * $h1);
        $c22 = 2/($h2 * $h2) + ($h1/$tau);

        for ($i = 0; $i < 7; $i++){
            for ($j = 0; $j < 11; $j++){
                if($j === 0){
                    $tmp_result['temperature'][1]['full'][$i][$j] = (int)$T1;
                } elseif ($j === 10){
                    $tmp_result['temperature'][1]['full'][$i][$j] = (int)$T2;
                } else {
                    $tmp_result['temperature'][1]['full'][$i][$j] = ((($T2 - $T1)/$l) * $j) + $T1;
                }
            }
        }

        $indexPrevLevel = 1;
        for ($l = 2; $l <=4 ; $l++) {
            for ($i = 0; $i < 7; $i++) {
                $index = 0;
                for ($j = 0; $j < 11; $j++) {
                    if ($j === 0) {
                        $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['alpha-' . $j] = 0;
                        $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['betta-' . $j] = (int)$T1;
                    } else {
                        $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['alpha-' . $j] = $b21 / ($c21 - $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['alpha-' . $index] * $a21);
                        $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['f-' . $j] = ($h1/$tau)*$tmp_result['temperature'][$indexPrevLevel]['full'][$i][$j];
                        $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['betta-' . $j] = ($a21 * $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['betta-' . $index] + $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['f-' . $j]) /($c21 - ($tmp_result['temperature'][$indexPrevLevel]['half'][$i]['alpha-' . $index] * $a21));
                        $index++;
                    }

                }
            }

            for ($i = 0; $i < 7; $i++) {
                for ($j = 10; $j >= 0; $j--) {
                    if ($j === 0) {
                        $tmp_result['temperature'][$l]['half'][$i][$j] = (int)$T1;
                    } elseif ($j === 10) {
                        $tmp_result['temperature'][$l]['half'][$i][$j] = (int)$T2;
                    } else {
                        $tmp_result['temperature'][$l]['half'][$i][$j] = $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['alpha-' . $j] * $tmp_result['temperature'][$indexPrevLevel]['full'][$i][$j] + $tmp_result['temperature'][$indexPrevLevel]['half'][$i]['betta-' . $j];
                    }
                }
            }


            for ($i = 0; $i < 10; $i++) {
                $index = 0;
                for ($j = 0; $j < 7; $j++) {
                    if ($j === 0) {
                        $tmp_result['temperature'][$l]['full'][$i]['alpha-' . $j] = 0;
                        $tmp_result['temperature'][$l]['full'][$i]['betta-' . $j] = (int)$T1;
                    } else {
                        $tmp_result['temperature'][$l]['full'][$i]['alpha-' . $j] = $b22 / ($c22 - $tmp_result['temperature'][$l]['full'][$i]['alpha-' . $index] * $a22);
                        $tmp_result['temperature'][$l]['full'][$i]['f-' . $j] = ($h1/$tau)*$tmp_result['temperature'][$l]['half'][$j][$i];
                        $tmp_result['temperature'][$l]['full'][$i]['betta-' . $j] = ($a22 * $tmp_result['temperature'][$l]['full'][$i]['betta-' . $index] + $tmp_result['temperature'][$l]['full'][$i]['f-' . $j]) /($c22 - ($tmp_result['temperature'][$l]['full'][$i]['alpha-' . $index] * $a22));
                        $index++;
                    }
                }
            }

            for ($i = 0; $i < 10; $i++) {
                for ($j = 6; $j >= 0; $j--) {
                    if ($j === 6) {
                        $tmp_result['temperature'][$l]['full'][$i][$j] = $tmp_result['temperature'][$l]['full'][$i]['alpha-' . $j] * ($tmp_result['temperature'][$l]['full'][$i]['betta-' . $j]/(1-$tmp_result['temperature'][$l]['full'][$i]['alpha-' . $j])) + $tmp_result['temperature'][$l]['full'][$i]['betta-' . $j];
//                        $tmp_result['temperature'][$l]['full'][$i][$j] = $tmp_result['temperature'][$l]['full'][$i]['alpha-' . $j] * ($T3) + $tmp_result['temperature'][$l]['full'][$i]['betta-' . $j];
                    } else {
                        $tmp_result['temperature'][$l]['full'][$i][$j] = $tmp_result['temperature'][$l]['full'][$i]['alpha-' . $j] * $tmp_result['temperature'][$indexPrevLevel]['full'][$j][$i] + $tmp_result['temperature'][$l]['full'][$i]['betta-' . $j];
                    }
                }
            }

            for ($i = 0; $i < 7; $i++){
                for ($j = 0; $j < 11; $j++){
                    if($j === 0){
                        $tmp_result['temperature'][$l]['full'][$i][$j] = (int)$T1;
                    } elseif ($j === 10){
                        $tmp_result['temperature'][$l]['full'][$i][$j] = (int)$T2;
                    } else {
                        $tmp_result['temperature'][$l]['full'][$i][$j] = $tmp_result['temperature'][$l]['full'][$j][$i];
                    }
                }
            }
            $indexPrevLevel++;

        }
//        for ($l = 1; $l <=4 ; $l++) {
//            for ($i = 1; $i <= 6; $i++) {
//                for ($j = 0; $j < 11; $j++) {
////                    $result['temperature'][$l][$i][$j] = $tmp_result['temperature'][$l]['full'][$i][$j];
//                }
//            }
//        }

        /**/


        $l = $request->l;
        for ($i = 0; $i < 7; $i++){
            for ($j = 0; $j < 11; $j++){
                if($j === 0){
                    $tmp_result[1]['full'][$i][$j] = $C1;
                } elseif ($j === 10){
                    $tmp_result[1]['full'][$i][$j] = $C2;
                } else {
                    $tmp_result[1]['full'][$i][$j] = ((($C2 - $C1)/$l) * $j) + $C1;
                }
            }
        }
        $indexPrevLevel = 1;
        for ($l = 2; $l <=4 ; $l++) {
            for ($i = 0; $i < 7; $i++) {
                $index = 0;
                for ($j = 0; $j < 11; $j++) {
                    if ($j === 0) {
                        $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $j] = 0;
                        $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $j] = $C1;
                    } elseif($j === 10){
                        $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $j] = $b1 / ($c1 - $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $index] * $a1);
//                        $tmp_result[$indexPrevLevel]['half'][$i]['f-' . $j] = ($y/(2*$D)) * $C + ($sigma/($tau * $D))*$tmp_result[$indexPrevLevel]['full'][$i][$j];
                        $prev = $j;
                        $next = $j;
                        $f1 = ($DT/$D) * ($tmp_result['temperature'][$indexPrevLevel]['full'][$i][(--$prev)] + 2*$tmp_result['temperature'][$indexPrevLevel]['full'][$i][$j] + $tmp_result['temperature'][$indexPrevLevel]['full'][$i][($next)]);
                        $tmp_result[$indexPrevLevel]['half'][$i]['f-' . $j] = ($y/(2*$D)) * $C + $f1 + ($sigma/($tau * $D))*$tmp_result[$indexPrevLevel]['full'][$i][$j];
                        $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $j] = ($a1 * $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $index] + $tmp_result[$indexPrevLevel]['half'][$i]['f-' . $j]) /($c1 - ($tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $index] * $a1));
                        $index++;
                    } else {
                        $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $j] = $b1 / ($c1 - $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $index] * $a1);
//                        $tmp_result[$indexPrevLevel]['half'][$i]['f-' . $j] = ($y/(2*$D)) * $C + ($sigma/($tau * $D))*$tmp_result[$indexPrevLevel]['full'][$i][$j];
                        $prev = $j;
                        $next = $j;
                        $f1 = ($DT/$D) * ($tmp_result['temperature'][$indexPrevLevel]['full'][$i][(--$prev)] + 2*$tmp_result['temperature'][$indexPrevLevel]['full'][$i][$j] + $tmp_result['temperature'][$indexPrevLevel]['full'][$i][(++$next)]);
                        $tmp_result[$indexPrevLevel]['half'][$i]['f-' . $j] = ($y/(2*$D)) * $C + $f1 + ($sigma/($tau * $D))*$tmp_result[$indexPrevLevel]['full'][$i][$j];
                        $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $j] = ($a1 * $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $index] + $tmp_result[$indexPrevLevel]['half'][$i]['f-' . $j]) /($c1 - ($tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $index] * $a1));
                        $index++;
                    }

                }
            }

            for ($i = 0; $i < 7; $i++) {
                for ($j = 10; $j >= 0; $j--) {
                    if ($j === 0) {
                        $tmp_result[$l]['half'][$i][$j] = $C1;
                    } elseif ($j === 10) {
                        $tmp_result[$l]['half'][$i][$j] = $C2;
                    } else {
                        $tmp_result[$l]['half'][$i][$j] = $tmp_result[$indexPrevLevel]['half'][$i]['alpha-' . $j] * $tmp_result[$indexPrevLevel]['full'][$i][$j] + $tmp_result[$indexPrevLevel]['half'][$i]['betta-' . $j];
                    }
                }
            }

            for ($i = 0; $i < 10; $i++) {
                $index = 0;
                for ($j = 0; $j < 7; $j++) {
                    if ($j === 0) {
                        $tmp_result[$l]['full'][$i]['alpha-' . $j] = 0;
                        $tmp_result[$l]['full'][$i]['betta-' . $j] = $C;
                    } elseif($j === 10){
                        $tmp_result[$l]['full'][$i]['alpha-' . $j] = $b2 / ($c2 - $tmp_result[$l]['full'][$i]['alpha-' . $index] * $a2);
                        $tmp_result[$l]['full'][$i]['f-' . $j] = ($y/(2*$D)) * $C + ($sigma/($tau * $D))*$tmp_result[$l]['half'][$j][$i];
                        $prev = $j;
                        $next = $j;
                        $f2 = ($DT/$D) * ($tmp_result['temperature'][$l]['half'][$j][(--$prev)] + 2*$tmp_result['temperature'][$l]['half'][$j][$i] + $tmp_result['temperature'][$l]['half'][$j][($next)]);
                        $tmp_result[$l]['full'][$i]['f-' . $j] = ($y/(2*$D)) * $C + $f2 + ($sigma/($tau * $D))*$tmp_result[$l]['half'][$j][$i];
                        $tmp_result[$l]['full'][$i]['betta-' . $j] = ($a2 * $tmp_result[$l]['full'][$i]['betta-' . $index] + $tmp_result[$l]['full'][$i]['f-' . $j]) /($c2 - ($tmp_result[$l]['full'][$i]['alpha-' . $index] * $a2));
                        $index++;
                    } else {
                        $tmp_result[$l]['full'][$i]['alpha-' . $j] = $b2 / ($c2 - $tmp_result[$l]['full'][$i]['alpha-' . $index] * $a2);
                        $tmp_result[$l]['full'][$i]['f-' . $j] = ($y/(2*$D)) * $C + ($sigma/($tau * $D))*$tmp_result[$l]['half'][$j][$i];
                        $prev = $j;
                        $next = $j;
                        $f2 = ($DT/$D) * ($tmp_result['temperature'][$l]['half'][$j][(--$prev)] + 2*$tmp_result['temperature'][$l]['half'][$j][$i] + $tmp_result['temperature'][$l]['half'][$j][(++$next)]);
                        $tmp_result[$l]['full'][$i]['f-' . $j] = ($y/(2*$D)) * $C + $f2 + ($sigma/($tau * $D))*$tmp_result[$l]['half'][$j][$i];
                        $tmp_result[$l]['full'][$i]['betta-' . $j] = ($a2 * $tmp_result[$l]['full'][$i]['betta-' . $index] + $tmp_result[$l]['full'][$i]['f-' . $j]) /($c2 - ($tmp_result[$l]['full'][$i]['alpha-' . $index] * $a2));
                        $index++;
                    }
                }
            }
            for ($i = 0; $i < 10; $i++) {
                for ($j = 6; $j >= 0; $j--) {
                    if ($j === 6) {
                        $tmp_result[$l]['full'][$i][$j] = $tmp_result[$l]['full'][$i]['alpha-' . $j] * ($tmp_result[$l]['full'][$i]['betta-' . $j]/(1-$tmp_result[$l]['full'][$i]['alpha-' . $j])) + $tmp_result[$l]['full'][$i]['betta-' . $j];
                    } else {
                        $tmp_result[$l]['full'][$i][$j] = $tmp_result[$l]['full'][$i]['alpha-' . $j] * $tmp_result[$indexPrevLevel]['full'][$j][$i] + $tmp_result[$l]['full'][$i]['betta-' . $j];
                    }
                }
            }
            for ($i = 0; $i < 7; $i++){
                for ($j = 0; $j < 11; $j++){
                    if($j === 0){
                        $tmp_result[$l]['full'][$i][$j] = $C1;
                    } elseif ($j === 10){
                        $tmp_result[$l]['full'][$i][$j] = $C2;
                    } else {
                        $tmp_result[$l]['full'][$i][$j] = $tmp_result[$l]['full'][$j][$i];
                    }
                }
            }
            $indexPrevLevel++;

        }


        for ($l = 1; $l <=4 ; $l++) {
            for ($i = 1; $i <= 6; $i++) {
                for ($j = 0; $j < 11; $j++) {
                    $result[$l][$i][$j] = $tmp_result[$l]['full'][$i][$j];
                    $result_temp[$l][$i][$j] = $tmp_result['temperature'][$l]['full'][$i][$j];
                }
            }
        }
        return view('lab4.char',compact('result','result_temp'));
    }

    public function lab5Index(){
        return view('lab5.index');
    }

    public function lab5Calc(Request $request){
        $H1 = $request->H1;
        $H2 = $request->H2;
        $C1 = $request->C1;
        $C2 = $request->C2;
        $D= $request->D;
        $k= $request->k;
        $v= $request->v;
        $a= $request->a;
        $e= $request->e;
        $y= $request->y;
        $l= $request->l;
        $C= $request->C;
        $sigma= $request->sigma;
        $h1= $request->h1;
        $Ygr= $request->Ygr;
        $tao= $request->tao;

        $U= -$k*(($H2-$H1)/$l);
        $V=(($k*($H1-$H2))/$l);
        $u = 1/(1+(($h1*$V)/(2*$D)));
        $r = -($V/$D);
        $r_minus = 0.5*($r - abs($r));
        $r_plus = 0.5*($r + abs($r));
        $a1 = ($u/($h1 * $h1))-($r_minus/$h1);
        $b1 = ($u/($h1 * $h1))+($r_plus/$h1);
        $c1 = ((2*$u)/($h1 * $h1)) +($r_plus/$h1) - ($r_minus/$h1) + ($y/($D)) + ($sigma/($D * $tao));
        $a_ser = $k* ((1+$e)/($Ygr*$a1));
        $b_ser = $v* ((1+$e)/($Ygr*$a1));

        $a2=$b2=$a_ser/($h1 * $h1);
        $c2 = 1/$tao - (2*$a)/($h1*$h1);
        $tmp_result = [];
        for ($j = 0; $j < 11; $j++){
            if($j === 0){
                $tmp_result['mass'][1][$j] = (int)$C1;
                $tmp_result['napir'][1][$j] = (int)$H1;
            } elseif ($j === 10){
                $tmp_result['mass'][1][$j] = (int)$C2;
                $tmp_result['napir'][1][$j] = (int)$H2;
            } else {
                $tmp_result['mass'][1][$j] = $C1 * exp((-($j/$l))*log($C1/$C2));
                $tmp_result['napir'][1][$j] = (($H2 - $H1)/$l)*$j + $H1;
            }
        }
        $indexPrevLevel = 1;
        for($level = 2; $level<=4; $level++){
            $index = 0;
            for ($j = 0; $j < 10; $j++) {
                if ($j === 0) {
                    $tmp_result['mass'][$indexPrevLevel]['alpha-' . $j] = 0;
                    $tmp_result['napir'][$indexPrevLevel]['alpha-' . $j] = 0;
                    $tmp_result['mass'][$indexPrevLevel]['betta-' . $j] = (int)$C1;
                    $tmp_result['napir'][$indexPrevLevel]['betta-' . $j] = (int)$H1;
                } else {
                    $tmp_result['mass'][$indexPrevLevel]['alpha-' . $j] = $b1 / ($c1 - $tmp_result['mass'][$indexPrevLevel]['alpha-' . $index] * $a1);
                    $tmp_result['mass'][$indexPrevLevel]['f-' . $j] = ($y/(2*$D)) * $C + ($sigma/($tao * $D))*$tmp_result['mass'][$indexPrevLevel][$j];
                    $tmp_result['mass'][$indexPrevLevel]['betta-' . $j] = ($a1 * $tmp_result['mass'][$indexPrevLevel]['betta-' . $index] + $tmp_result['mass'][$indexPrevLevel]['f-' . $j]) /($c1 - ($tmp_result['mass'][$indexPrevLevel]['alpha-' . $index] * $a1));


                    $tmp_result['napir'][$indexPrevLevel]['alpha-' . $j] = $b2 / ($c2 - $tmp_result['napir'][$indexPrevLevel]['alpha-' . $index] * $a2);
                    $prev = $j;
                    $next = $j;
                    $tmp_result['napir'][$indexPrevLevel]['f-' . $j] = (1/$tao) * $tmp_result['napir'][$indexPrevLevel][$j] + $b_ser * (($tmp_result['mass'][$indexPrevLevel][(--$prev)] - $tmp_result['mass'][$indexPrevLevel][$j] + $tmp_result['mass'][$indexPrevLevel][(++$next)])/($h1*$h1));
                    $tmp_result['napir'][$indexPrevLevel]['betta-' . $j] = ($a2 * $tmp_result['napir'][$indexPrevLevel]['betta-' . $index] + $tmp_result['napir'][$indexPrevLevel]['f-' . $j]) /($c2 - ($tmp_result['napir'][$indexPrevLevel]['alpha-' . $index] * $a2));

                    $index++;
                }
            }
            for ($j = 0; $j < 11; $j++){
                if($j === 0){
                    $tmp_result['mass'][$level][$j] = (int)$C1;
                    $tmp_result['napir'][$level][$j] = (int)$H1;
                } elseif ($j === 10){
                    $tmp_result['mass'][$level][$j] = (int)$C2;
                    $tmp_result['napir'][$level][$j] = (int)$H2;
                } else {
                    $tmp_result['mass'][$level][$j] = $tmp_result['mass'][$indexPrevLevel]['alpha-' . $j] * $tmp_result['mass'][$indexPrevLevel][$j] + $tmp_result['mass'][$indexPrevLevel]['betta-' . $j];
                    $tmp_result['napir'][$level][$j] = $tmp_result['napir'][$indexPrevLevel]['alpha-' . $j] * $tmp_result['napir'][$indexPrevLevel][$j] + $tmp_result['napir'][$indexPrevLevel]['betta-' . $j];
                }
            }
            $indexPrevLevel++;
        }
        for($level = 1; $level<=4; $level++) {
            for ($j = 0; $j < 11; $j++) {
                $result['mass'][$level][$j] = $tmp_result['mass'][$level][$j];
                $result['napir'][$level][$j] = $tmp_result['napir'][$level][$j];
            }
        }
        $xAxis = [10,20,30,40,50,60,70,80,90,100];
        $chartjs = app()->chartjs
            ->name('Chart1')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($xAxis)
            ->datasets([
                [
                    "label" => "Нульовий рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['mass'][1]),
                ],
                [
                    "label" => "Перший рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['mass'][2]),
                ],
                [
                    "label" => "Другий рівень",
                    'backgroundColor' => "rgba(98, 181, 31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['mass'][3]),
                ],
                [
                    "label" => "Третій рівень",
                    'backgroundColor' => "rgba(47, 161, 157)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['mass'][4]),
                ]
            ])
            ->options([]);
        $chartjs1 = app()->chartjs
            ->name('Chart2')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($xAxis)
            ->datasets([
                [
                    "label" => "Нульовий рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['napir'][1]),
                ],
                [
                    "label" => "Перший рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['napir'][2]),
                ],
                [
                    "label" => "Другий рівень",
                    'backgroundColor' => "rgba(98, 181, 31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['napir'][3]),
                ],
                [
                    "label" => "Третій рівень",
                    'backgroundColor' => "rgba(47, 161, 157)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['napir'][4]),
                ]
            ])
            ->options([]);
        return view('lab5.chart',compact('chartjs','chartjs1'))->with(['mass'=>$result['mass'],'napir'=>$result['napir']]);

    }

    public function lab7index(){
        return view('lab7.index');
    }

    public function lab7Calc(Request $request){
        $Pp = $request->Pp;
        $Pg = $request->Pg;
        $E = $request->E;
        $sigma = $request->sigma;
        $H1 = $request->H1;
        $H2 = $request->H2;
        $L = $request->L;
        $g = $request->g;
        $h = $request->h;

        $lamda = ($E*$sigma)/((1+$sigma)*(1-2*$sigma));
        $u = $E / (2*(1+$sigma));
//        сухий
        $a1 = ($Pg * $g)/($lamda+2*$u);
//        зволожений
        $a2 = (($Pg - $Pp)*$g)/($lamda+2*$u);
//        Процес фільтації
        $a3 = (($Pg+(($H2 - $H1)/$L - 1)*$Pp)*$g)/($lamda+2*$u);
        $result = [];
        for($i = 1; $i < 21; $i++){
            $result['u']['dry'][$i] = 0.5*$a1*($i*$h) * ($i*$h-1);
            $result['u']['dewy'][$i] = 0.5*$a2*($i*$h) * ($i*$h-1);
            $result['u']['filtration'][$i] = 0.5*$a3*($i*$h) * ($i*$h-1);

            $result['eps']['dry'][$i] = $a1*$i*$h-$a1/2;
            $result['eps']['dewy'][$i] = $a2*$i*$h-$a2/2;
            $result['eps']['filtration'][$i] = $a3*$i*$h-$a3/2;

            $result['sigma']['dry'][$i] = ($lamda*2*$u)*$result['eps']['dry'][$i];
            $result['sigma']['dewy'][$i] = ($lamda*2*$u)*$result['eps']['dewy'][$i];
            $result['sigma']['filtration'][$i] = ($lamda*2*$u)*$result['eps']['filtration'][$i];
        }


        $chartjs = app()->chartjs
            ->name('lineChartTest')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels(array_keys( $result['u']['dry']))
            ->datasets([
                [
                    "label" => "Сухий",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['u']['dry']),
                ],
                [
                    "label" => "Зволожений",
                    'backgroundColor' => " ",
                    'borderColor' => "#F81009 ",
                    "pointBorderColor" => "#F81009 ",
                    "pointBackgroundColor" => "#F81009 ",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#F81009 ",
                    'data' => array_values($result['u']['dewy']),
                ],
                [
                    "label" => "Процес фільтрації",
                    'backgroundColor' => "",
                    'borderColor' => "#3BF809",
                    "pointBorderColor" => "#3BF809",
                    "pointBackgroundColor" => "#3BF809",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#3BF809",
                    'data' => array_values($result['u']['filtration']),
                ]
            ])
            ->options([]);

        $chartjs_1 = app()->chartjs
            ->name('lineChartTest1')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels(array_keys( $result['u']['dry']))
            ->datasets([
                [
                    "label" => "Сухий",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['eps']['dry']),
                ],
                [
                    "label" => "Зволожений",
                    'backgroundColor' => " ",
                    'borderColor' => "#F81009 ",
                    "pointBorderColor" => "#F81009 ",
                    "pointBackgroundColor" => "#F81009 ",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#F81009 ",
                    'data' => array_values($result['eps']['dewy']),
                ],
                [
                    "label" => "Процес фільтрації",
                    'backgroundColor' => "",
                    'borderColor' => "#3BF809",
                    "pointBorderColor" => "#3BF809",
                    "pointBackgroundColor" => "#3BF809",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#3BF809",
                    'data' => array_values($result['eps']['filtration']),
                ]
            ])
            ->options([]);

        $chartjs_2 = app()->chartjs
            ->name('lineChartTest2')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels(array_keys( $result['u']['dry']))
            ->datasets([
                [
                    "label" => "Сухий",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['sigma']['dry']),
                ],
                [
                    "label" => "Зволожений",
                    'backgroundColor' => " ",
                    'borderColor' => "#F81009 ",
                    "pointBorderColor" => "#F81009 ",
                    "pointBackgroundColor" => "#F81009 ",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#F81009 ",
                    'data' => array_values($result['sigma']['dewy']),
                ],
                [
                    "label" => "Процес фільтрації",
                    'backgroundColor' => "",
                    'borderColor' => "#3BF809",
                    "pointBorderColor" => "#3BF809",
                    "pointBackgroundColor" => "#3BF809",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#3BF809",
                    'data' => array_values($result['sigma']['filtration']),
                ]
            ])
            ->options([]);

        return view('lab7.chart', compact('chartjs','chartjs_1','chartjs_2'));
    }

    public function lab8index(){
        return view('lab8.index');
    }

    public function lab8Calc(Request $request){
        $Pp = $request->Pp;
        $P1= $request->P1;
        $P2= $request->P2;
        $lam1= $request->lam1;
        $lam2= $request->lam2;
        $u1= $request->u1;
        $u2= $request->u2;
        $E1= $request->E1;
        $E2= $request->E2;
        $L1= $request->L1;
        $L2= $request->L2;
        $g = $request->g;

        $yPR = 17;
        $yZV = 10.5;
//        $a1 = ($Pp + $P1*(($H2-$H1)/$E2-1))*$g/($lam1+2*$u1);
//        $a2 = $P2*$g/($lam2+2*$u2);
        $a1 = $yZV/($lam1+2*$u1);
        $a2 = $yPR/($lam2+2*$u2);
        $c2=0;
        $c3= (($a2/2)-((($a1+$a2)*($L1*$L1))/2) + ((($lam2+2*$u2)/($lam1+2*$u1))*($a2*($L1*$L1)))) / ( ( 1 - (($lam2+2*$u2)/($lam1+2*$u1)))*$L1 - $L2);
        $c1 = (($lam2+2*$u2)/($lam1+2*$u1))*($a2*$L1+$c3) - $a1*$L1;
        $c4 = -$c3*$L1-(($a2*($L1*$L1))/2);
        $result = [];
        $keys = [];
        for($i = 0; $i <= $L2; $i+=0.5){
            $keys[] = $i;
            if($i <= $L1){
                $result['u1'][] = ($a1*($i*$i))/2 + $c1*$i + $c2;
                $result['eps1'][] = $a1*$i+$c1;
                $result['sigma1'][] = ($lam1 + 2*$u1)*($a1*$i+$c1);
            } else {
                /*$result['u2'][] = ($a2*($i*$i))/2 + $c3*$i + $c4;
                $result['eps2'][] = $a2*$i+$c3;
                $result['sigma2'][] = ($lam2 + 2*$u2)*($a2*$i+$c3);*/
                $result['u1'][] = ($a2*($i*$i))/2 + $c3*$i + $c4;
                $result['eps1'][] = $a2*$i+$c3;
                $result['sigma1'][] = ($lam2 + 2*$u2)*($a2*$i+$c3);
            }
        }
        $chartjs = app()->chartjs
            ->name('lineChartTest')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($keys)
            ->datasets([
                [
                    "label" => "U1",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['u1']),
                ],
//                [
//                    "label" => "U2",
//                    'backgroundColor' => " ",
//                    'borderColor' => "#F81009 ",
//                    "pointBorderColor" => "#F81009 ",
//                    "pointBackgroundColor" => "#F81009 ",
//                    "pointHoverBackgroundColor" => "#fff",
//                    "pointHoverBorderColor" => "#F81009 ",
//                    'data' => array_values($result['u2']),
//                ]
            ])
            ->options([]);
        $chartjs_1 = app()->chartjs
            ->name('lineChartTest1')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($keys)
            ->datasets([
                [
                    "label" => "E1",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['eps1']),
                ],
//                [
//                    "label" => "E2",
//                    'backgroundColor' => " ",
//                    'borderColor' => "#F81009 ",
//                    "pointBorderColor" => "#F81009 ",
//                    "pointBackgroundColor" => "#F81009 ",
//                    "pointHoverBackgroundColor" => "#fff",
//                    "pointHoverBorderColor" => "#F81009 ",
//                    'data' => array_values($result['eps2']),
//                ]
            ])
            ->options([]);

        $chartjs_2 = app()->chartjs
            ->name('lineChartTest2')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($keys)
            ->datasets([
                [
                    "label" => "Sigma1",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['sigma1']),
                ],
//                [
//                    "label" => "Sigma2",
//                    'backgroundColor' => " ",
//                    'borderColor' => "#F81009 ",
//                    "pointBorderColor" => "#F81009 ",
//                    "pointBackgroundColor" => "#F81009 ",
//                    "pointHoverBackgroundColor" => "#fff",
//                    "pointHoverBorderColor" => "#F81009 ",
//                    'data' => array_values($result['sigma2']),
//                ]
            ])
            ->options([]);

        return view('lab7.chart', compact('chartjs','chartjs_1','chartjs_2'));

    }

    public function lab9index(){
        return view('lab9.index');
    }

    public function lab9Calc(Request $request){
        $D = $request->D;
        $Q = $request->Q;
        $y = $request->y;
        $l = $request->l;
        $X0 = $request->X0;

        $w = ($y/$D)**(1/2);

        $X01_Sim = 7.18;
        $A_0 = $Q/($w*$D*sinh($w)*$l);
        $result = [];
        $keys = [];
        $k = 0;
        for($i = 0; $i <= 10; $i+=0.01){
            if($i == 0){
                $result['C(x,x0)']['('.number_format($i,2).')'] = 0;
                $result['C(x,x01(Sim))']['('.number_format($i,2).')'] = 0;
                $result['sr_snach'] = 0;
            } else {
                if($i<=$X0){
                    $result['C(x,x0)']['('.number_format($i,2).')'] = ($Q/($w*$D*sinh($w*$l)))*sinh($w*($l-$X0))*sinh($w*$i);

                } else {
                    $result['C(x,x0)']['('.number_format($i,2).')'] = ($Q/($w*$D*sinh($w*$l)))*sinh($w*($l-$i))*sinh($w*$X0);
                }

                if($i<= 10 - $X0){
                    $result['C(x,x01(Sim))']['('.number_format($i,2).')'] = ($Q/($w*$D*sinh($w*$l)))*sinh($w*($l-$X01_Sim))*sinh($w*$i);
                }else{
                    $result['C(x,x01(Sim))']['('.number_format($i,2).')'] = ($Q/($w*$D*sinh($w*$l)))*sinh($w*($l-$i))*sinh($w*$X01_Sim);
                }
            }

            $result['sr_snach']+= $result['C(x,x0)']['('.number_format($i,2).')'];
            $keys[] = $k++;
        }


        $my_points = [
            4.44,
            5,
            6.32,
            7.77
        ];
        foreach ($my_points as $key => $item){
            for($i = 0; $i <= 10; $i+=0.01){
                if($i == 0){
                    $result['('.number_format($item,2).')']['C(x,x0)']['('.number_format($i,2).')'] = 0;
                    $result['('.number_format($item,2).')']['C(x,x01(Sim))']['('.number_format($i,2).')'] = 0;
                    $result['('.number_format($item,2).')']['sr_snach'] = 0;
                } else {
                    if($i<=$item){
                        $result['('.number_format($item,2).')']['C(x,x0)']['('.number_format($i,2).')'] = ($Q/($w*$D*sinh($w*$l)))*sinh($w*($l-$item))*sinh($w*$i);

                    } else {
                        $result['('.number_format($item,2).')']['C(x,x0)']['('.number_format($i,2).')'] = ($Q/($w*$D*sinh($w*$l)))*sinh($w*($l-$i))*sinh($w*$item);
                    }

                    if($i<= 10 - $item){
                        $result['('.number_format($item,2).')']['C(x,x01(Sim))']['('.number_format($i,2).')'] = ($Q/($w*$D*sinh($w*$l)))*sinh($w*($l-(10-$item)))*sinh($w*$i);
                    }else{
                        $result['('.number_format($item,2).')']['C(x,x01(Sim))']['('.number_format($i,2).')'] = ($Q/($w*$D*sinh($w*$l)))*sinh($w*($l-$i))*sinh($w*(10-$item));
                    }
                }
                $result['('.number_format($item,2).')']['sr_snach']+= $result['C(x,x0)']['('.number_format($i,2).')'];
            }
        }
        $chartjs = app()->chartjs
            ->name('lineChartTest')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($keys)
            ->datasets([
                [
                    "label" => "C(x,x0)",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['C(x,x0)']),
                ],
                [
                    "label" => "C(x,x01(Sim))",
                    'backgroundColor' => " ",
                    'borderColor' => "#F81009 ",
                    "pointBorderColor" => "#F81009 ",
                    "pointBackgroundColor" => "#F81009 ",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#F81009 ",
                    'data' => array_values($result['C(x,x01(Sim))']),
                ]
            ])
            ->options([]);
        $chartjs1 = app()->chartjs
            ->name('lineChartTest1')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($keys)
            ->datasets([
                [
                    "label" => "C(x,x0) x = 4.44",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['(4.44)']['C(x,x0)']),
                ],
                [
                    "label" => "C(x,x01(Sim)) x = 4.44 ",
                    'backgroundColor' => "",
                    'borderColor' => "#09B3F8",
                    "pointBorderColor" => "#09B3F8",
                    "pointBackgroundColor" => "#09B3F8",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09B3F8",
                    'data' => array_values($result['(4.44)']['C(x,x01(Sim))']),
                ],
////////////////////////////////////////
                [
                    "label" => "C(x,x0) x = 5",
                    'backgroundColor' => "",
                    'borderColor' => "#09F8C6",
                    "pointBorderColor" => "#09F8C6",
                    "pointBackgroundColor" => "#09F8C6",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09F8C6",
                    'data' => array_values($result['(5.00)']['C(x,x0)']),
                ],
                [
                    "label" => "C(x,x01(Sim)) x = 5 ",
                    'backgroundColor' => "",
                    'borderColor' => "#09F8C6",
                    "pointBorderColor" => "#09F8C6",
                    "pointBackgroundColor" => "#09F8C6",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#09F8C6",
                    'data' => array_values($result['(5.00)']['C(x,x01(Sim))']),
                ],
////////////////////////////////////////
                [
                    "label" => "C(x,x0) x = 6.32",
                    'backgroundColor' => "",
                    'borderColor' => "#9BF085",
                    "pointBorderColor" => "#9BF085",
                    "pointBackgroundColor" => "#9BF085",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#9BF085",
                    'data' => array_values($result['(6.32)']['C(x,x0)']),
                ],
                [
                    "label" => "C(x,x01(Sim)) x = 6.32 ",
                    'backgroundColor' => "",
                    'borderColor' => "#9BF085",
                    "pointBorderColor" => "#9BF085",
                    "pointBackgroundColor" => "#9BF085",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#9BF085",
                    'data' => array_values($result['(6.32)']['C(x,x01(Sim))']),
                ],
////////////////////////////////////////
                [
                    "label" => "C(x,x0) x = 7.77",
                    'backgroundColor' => "",
                    'borderColor' => "#E8845B ",
                    "pointBorderColor" => "#E8845B ",
                    "pointBackgroundColor" => "#E8845B ",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#E8845B ",
                    'data' => array_values($result['(7.77)']['C(x,x0)']),
                ],
                [
                    "label" => "C(x,x01(Sim)) x = 7.77 ",
                    'backgroundColor' => "",
                    'borderColor' => "#E8845B ",
                    "pointBorderColor" => "#E8845B ",
                    "pointBackgroundColor" => "#E8845B ",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "#E8845B ",
                    'data' => array_values($result['(7.77)']['C(x,x01(Sim))']),
                ]
            ])
            ->options([]);
        $x0 = $l-1/$w*asinh( $result['C(x,x0)']['('.$X0.')']/($A_0*sinh($w*$X0)));
        $x0_tilda = $l-1/$w*asinh( ($result['sr_snach']/1001)/($A_0*sinh($w*$X0)));
        $my_points_x = [];
        foreach ($my_points as $key => $item){
            if($item <= 5){
//                array_push($my_points_x,[
//                    '('.$item.')'=>[
//                        'x'=>$l-1/$w*asinh( $result['('.number_format($item,2).')']['C(x,x0)']['('.number_format($item,2).')']/($A_0*sinh($w*$item))),
//                        'x_tilda' => $l-1/$w*asinh( ($result['('.number_format($item,2).')']['sr_snach']/1001)/($A_0*sinh($w*$item))),
//                    ]
//                ]);
                $my_points_x['('.number_format($item,2).')']['x']= $l-1/$w*asinh( $result['('.number_format($item,2).')']['C(x,x0)']['('.number_format($item,2).')']/($A_0*sinh($w*$item)));
                $my_points_x['('.number_format($item,2).')']['x_tilda']= $l-1/$w*asinh( ($result['('.number_format($item,2).')']['sr_snach']/1001)/($A_0*sinh($w*$item)));
            }else{
//                array_push($my_points_x,[
//                    '('.$item.')'=>[
//                        'x'=>1/$w*asinh( $result['('.number_format($item,2).')']['C(x,x0)']['('.number_format($item,2).')']/($A_0*sinh($w*($l - $item)))),
//                        'x_tilda' => 1/$w*asinh( ($result['('.number_format($item,2).')']['sr_snach']/1001)/($A_0*sinh($w*($l - $item)))),
//                    ]
//                ]);
                $my_points_x['('.number_format($item,2).')']['x']= 1/$w*asinh( $result['('.number_format($item,2).')']['C(x,x0)']['('.number_format($item,2).')']/($A_0*sinh($w*($l - $item))));
                $my_points_x['('.number_format($item,2).')']['x_tilda']= 1/$w*asinh( ($result['('.number_format($item,2).')']['sr_snach']/1001)/($A_0*sinh($w*($l - $item))));
            }
        }
        return view('lab9.chart', compact('chartjs','x0','x0_tilda','chartjs1','my_points_x'));
    }

    public function lab6index(){
        return view('lab6.index');
    }
    public function lab6Calc(Request $request){
        $H1 = $request->H1;
        $H2 = $request->H2;
        $C1 = $request->C1;
        $C2 = $request->C2;
        $D= $request->D;
        $v= $request->v;
        $T= $request->T;
        $l= $request->l;
        $y= $request->y;
        $lamda= $request->lamda;
        $p= $request->p;
        $C= $request->C;
        $C0= $request->C0;
        $Sigma= $request->Sigma;
        $h1 = 10;
        $k = 0.001;
        $tau = 30;

        $V=(($H1-$H2)/$l) * $k;
        $u = 1/(1+(($h1*$V)/(2*$D)));
        $r = -($V/$D);
        $r_minus = 0.5*($r - abs($r));
        $r_plus = 0.5*($r + abs($r));
        $a1 = ($u/($h1 * $h1))-($r_minus/$h1);
        $b1 = ($u/($h1 * $h1))+($r_plus/$h1);
        $c1 = ((2*$u)/($h1 * $h1)) +($r_plus/$h1) - ($r_minus/$h1) + ($y/($D)) + ($Sigma/($D * $tau));
        $ab = $k/($h1 * $h1);
        $g=9.8;

        $tmp_result = [];
        for ($j = 0; $j < 11; $j++){
            if($j === 0){
                $tmp_result['mass'][1][$j] = (int)$C1;
                $tmp_result['napir'][1][$j] = (int)$H1;
            } elseif ($j === 10){
                $tmp_result['mass'][1][$j] = (int)$C2;
                $tmp_result['napir'][1][$j] = (int)$H2;
            } else {
                $tmp_result['mass'][1][$j] = (int)$C0;
                $tmp_result['napir'][1][$j] = (($H2 - $H1)/$l)*$j + $H1;
            }
        }
        $indexPrevLevel = 1;
        for($level = 2; $level<=4; $level++){
            $index = 0;
            for ($j = 0; $j < 10; $j++) {
                if ($j === 0) {
                    $tmp_result['mass'][$indexPrevLevel]['alpha-' . $j] = 0;
                    $tmp_result['napir'][$indexPrevLevel]['alpha-' . $j] = 0;
                    $tmp_result['mass'][$indexPrevLevel]['betta-' . $j] = (int)$C1;
                    $tmp_result['napir'][$indexPrevLevel]['betta-' . $j] = (int)$H1;
                } else {
                    $tmp_result['mass'][$indexPrevLevel]['alpha-' . $j] = $b1 / ($c1 - $tmp_result['mass'][$indexPrevLevel]['alpha-' . $index] * $a1);
                    $tmp_result['mass'][$indexPrevLevel]['f-' . $j] = $y*$C + ($Sigma/($tau))*$tmp_result['mass'][$indexPrevLevel][$j];
                    $tmp_result['mass'][$indexPrevLevel]['betta-' . $j] = ($a1 * $tmp_result['mass'][$indexPrevLevel]['betta-' . $index] + $tmp_result['mass'][$indexPrevLevel]['f-' . $j]) /($c1 - ($tmp_result['mass'][$indexPrevLevel]['alpha-' . $index] * $a1));

                    $prev = $j;
                    $next = $j;
                    $u1 = $ab*$p*$g*(1-((2*$h1)/($tmp_result['napir'][$indexPrevLevel][(--$prev)] -  $tmp_result['napir'][$indexPrevLevel][(++$next)])));
                    $prev = $j;
                    $next = $j;
                    $c = ($u1/$tau) + ((2*$k)/($h1*$h1));
                    $tmp_result['napir'][$indexPrevLevel]['f-' . $j] = ($u1/$tau) * $tmp_result['napir'][$indexPrevLevel][$j] - ($v/($h1*$h1))*(($tmp_result['mass'][$indexPrevLevel][(--$prev)] - 2*$tmp_result['mass'][$indexPrevLevel][$j] + $tmp_result['mass'][$indexPrevLevel][(++$next)]));
                    $tmp_result['napir'][$indexPrevLevel]['alpha-' . $j] = $ab / ($c - $tmp_result['napir'][$indexPrevLevel]['alpha-' . $index] * $ab);
                    $tmp_result['napir'][$indexPrevLevel]['betta-' . $j] = ($ab * $tmp_result['napir'][$indexPrevLevel]['betta-' . $index] + $tmp_result['napir'][$indexPrevLevel]['f-' . $j]) /($c - ($tmp_result['napir'][$indexPrevLevel]['alpha-' . $index] * $ab));

                    $index++;
                }
            }
            for ($j = 0; $j < 11; $j++){
                if($j === 0){
                    $tmp_result['mass'][$level][$j] = (int)$C1;
                    $tmp_result['napir'][$level][$j] = (int)$H1;
                } elseif ($j === 10){
                    $tmp_result['mass'][$level][$j] = (int)$C2;
                    $tmp_result['napir'][$level][$j] = (int)$H2;
                } else {
                    $tmp_result['mass'][$level][$j] = $tmp_result['mass'][$indexPrevLevel]['alpha-' . $j] * $tmp_result['mass'][$indexPrevLevel][$j] + $tmp_result['mass'][$indexPrevLevel]['betta-' . $j];

                    $tmp_result['napir'][$level][$j] = $tmp_result['napir'][$indexPrevLevel]['alpha-' . $j] * $tmp_result['napir'][$indexPrevLevel][$j] + $tmp_result['napir'][$indexPrevLevel]['betta-' . $j];
                }
            }
            $indexPrevLevel++;
        }
        for($level = 1; $level<=4; $level++) {
            for ($j = 0; $j < 11; $j++) {
                $result['mass'][$level][$j] = $tmp_result['mass'][$level][$j];
                $result['napir'][$level][$j] = $tmp_result['napir'][$level][$j];
//                isset($tmp_result['v'][$level][$j]) ? $result['v'][$level][$j] = $tmp_result['v'][$level][$j] : 0;
                if($j > 0 && $j < 10){
                    $prev = $j;
                    $next = $j;
                    $h_tmp = -$k*(($tmp_result['napir'][$indexPrevLevel][(--$prev)] -  $tmp_result['napir'][$indexPrevLevel][(++$next)])/(2*$h1));
                    $prev = $j;
                    $next = $j;
                    $c_tmp = $v*(($tmp_result['mass'][$indexPrevLevel][(--$prev)] - 2*$tmp_result['mass'][$indexPrevLevel][$j] + $tmp_result['mass'][$indexPrevLevel][(++$next)])/($h1*$h1));
                    $result['v'][$level][$j] =  $h_tmp + $c_tmp;
                }
            }
        }

        $xAxis = [10,20,30,40,50,60,70,80,90,100];
        $chartjs = app()->chartjs
            ->name('Chart1')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($xAxis)
            ->datasets([
                [
                    "label" => "Нульовий рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['mass'][1]),
                ],
                [
                    "label" => "Перший рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['mass'][2]),
                ],
                [
                    "label" => "Другий рівень",
                    'backgroundColor' => "rgba(98, 181, 31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['mass'][3]),
                ],
                [
                    "label" => "Третій рівень",
                    'backgroundColor' => "rgba(47, 161, 157)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['mass'][4]),
                ]
            ])
            ->options([]);

        $chartjs1 = app()->chartjs
            ->name('Chart2')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($xAxis)
            ->datasets([
                [
                    "label" => "Нульовий рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['napir'][1]),
                ],
                [
                    "label" => "Перший рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['napir'][2]),
                ],
                [
                    "label" => "Другий рівень",
                    'backgroundColor' => "rgba(98, 181, 31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['napir'][3]),
                ],
                [
                    "label" => "Третій рівень",
                    'backgroundColor' => "rgba(47, 161, 157)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['napir'][4]),
                ]
            ])
            ->options([]);

        $chartjs2 = app()->chartjs
            ->name('Chart3')
            ->type('line')
            ->size(['width' => 400, 'height' => 200])
            ->labels($xAxis)
            ->datasets([
                [
                    "label" => "Нульовий рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['v'][1]),
                ],
                [
                    "label" => "Перший рівень",
                    'backgroundColor' => "rgba(252, 186, 3)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['v'][2]),
                ],
                [
                    "label" => "Другий рівень",
                    'backgroundColor' => "rgba(98, 181, 31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['v'][3]),
                ],
                [
                    "label" => "Третій рівень",
                    'backgroundColor' => "rgba(47, 161, 157)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => array_values($result['v'][4]),
                ]
            ])
            ->options([]);
        return view('lab6.chart',compact('chartjs','chartjs1','chartjs2'))->with(['mass'=>$result['mass'],'napir'=>$result['napir'],'v'=>$result['v']]);
    }
}
