<?php
/**
 * Created by PhpStorm.
 * User: Sokol
 * Date: 02.11.2020
 * Time: 15:30
 */

namespace App\Http\Controllers;


class TextString
{
    private $string;

    public function __construct($string)
    {
        $this->string = $string;
    }

    public function __destruct()
    {
        unset($this->string);
    }

    public function getString(){
        return $this->string;
    }

    public function clearString(){
        $this->string = '';
    }

    public function searchInString($search){
        return strpos($this->string,$search);
    }
}