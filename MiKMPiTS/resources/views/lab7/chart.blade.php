@extends('layout.app')

@section('content')
    <h3>Математичне та комп'ютерне моделювання НДС в шарі грунту
        в одновимірному випадку</h3>
    <div>
        <h3>Переміщення</h3>
        <div style="width:100%;">
            {!! $chartjs->render() !!}
        </div>
    </div>
    <div>
        <h3>Деформація</h3>
        <div style="width:100%;">
            {!! $chartjs_1->render() !!}
        </div>
    </div>
    <div>
        <h3>Напруження</h3>
        <div style="width:100%;">
            {!! $chartjs_2->render() !!}
        </div>
    </div>
@endsection