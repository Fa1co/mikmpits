@extends('layout.app')

@section('content')
    <h3>Математичне та комп'ютерне моделювання НДС в шарі грунту
        в одновимірному випадку</h3>
    <form action="{{route('lab7.calc')}}" method="post">
        @csrf
        <div class="form-group row">
            <div class="col-2"><label for="Pp">Pp</label></div>
            <div class="col-8"><input type="number" class="form-control" id="Pp" name="Pp" value="2000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="Pg">Pg</label></div>
            <div class="col-8"><input type="number" class="form-control" id="Pg" name="Pg" value="2200" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="E">E</label></div>
            <div class="col-8"><input type="number" class="form-control" id="E" name="E" value="2500000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="sigma">sigma</label></div>
            <div class="col-8"><input type="number" class="form-control" id="sigma" name="sigma" value="0.35" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="H1">H1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H1" name="H1" value="1" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="H2">H2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H2" name="H2" value="5" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="L">L</label></div>
            <div class="col-8"><input type="number" class="form-control" id="L" name="L" value="5" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="g">g</label></div>
            <div class="col-8"><input type="number" class="form-control" id="g" name="g" value="9.8" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="h">h</label></div>
            <div class="col-8"><input type="number" class="form-control" id="h" name="h" value="0.05" required step=".00000000001"></div>
        </div>

        <button type="submit" class="btn btn-primary">Виконати розрахунок</button>
    </form>
@endsection