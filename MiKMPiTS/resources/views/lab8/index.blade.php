@extends('layout.app')

@section('content')
    <h3>Математичне моделювання НДС в шарі грунту при наявності вільної поверхні (рівня ґрунтових вод)</h3>
    <form action="{{route('lab8.calc')}}" method="post">
        @csrf
        <div class="form-group row">
            <div class="col-2"><label for="Pp">Pp</label></div>
            <div class="col-8"><input type="number" class="form-control" id="Pp" name="Pp" value="1000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="P1">P1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="P1" name="P1" value="1950" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="P2">P2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="P2" name="P2" value="1650" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="lam1">lam1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="lam1" name="lam1" value="2160000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="lam2">lam2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="lam2" name="lam2" value="2950000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="u1">u1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="u1" name="u1" value="9260000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="u2">u2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="u2" name="u2" value="1110000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="E1">E1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="E1" name="E1" value="2500000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="E2">E2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="E2" name="E2" value="3000000" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="L1">L1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="L1" name="L1" value="7" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="L2">L2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="L2" name="L2" value="10" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="g">g</label></div>
            <div class="col-8"><input type="number" class="form-control" id="g" name="g" value="9.8" required step=".00000000001"></div>
        </div>


        <button type="submit" class="btn btn-primary">Виконати розрахунок</button>
    </form>
@endsection