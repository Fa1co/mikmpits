@extends('layout.app')

@section('content')
<h3>>Математичне моделювання НДС в шарі грунту при наявності вільної поверхні (рівня ґрунтових вод)</h3>
<div>
    <h3>Переміщення</h3>
    <div style="width:100%;">
        {!! $chartjs->render() !!}
    </div>
</div>
<div>
    <h3>Деформація</h3>
    <div style="width:100%;">
        {!! $chartjs_1->render() !!}
    </div>
</div>
<div>
    <h3>Напруження</h3>
    <div style="width:100%;">
        {!! $chartjs_2->render() !!}
    </div>
</div>
@endsection