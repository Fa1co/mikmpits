@extends('layout.app')

@section('content')
    <h3>Математичне і комп’ютерне моделювання ідентифікації місцеположення джерела забруднення в одновимірній задачі масопереносу</h3>
    <div>
        <h3>За заданим x0</h3>
        <div style="width:100%;">
            {!! $chartjs->render() !!}
        </div>
    </div>
    <div style="font-weight: bold">
        x0 = {{$x0}}
        x0~ = {{$x0_tilda}}
    </div>
    <div>
        <h3>За выбраними х</h3>
        <div style="width:100%;">
            {!! $chartjs1->render() !!}
        </div>
    </div>
    <div style="font-weight: bold">
        @foreach($my_points_x as $key => $item)
          <div>
              <h5>{{$key}}</h5>
                <p>
                    x0 = {{$item['x']}} <br>
                    x0~ = {{$item['x_tilda']}}
                </p>
          </div>
        @endforeach
    </div>
@endsection