@extends('layout.app')

@section('content')
    <h3>Математичне і комп’ютерне моделювання ідентифікації місцеположення джерела забруднення в одновимірній задачі масопереносу</h3>
    <form action="{{route('lab9.calc')}}" method="post">
        @csrf
        <div class="form-group row">
            <div class="col-2"><label for="D">D</label></div>
            <div class="col-8"><input type="number" class="form-control" id="D" name="D" value="1" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="Q">Q</label></div>
            <div class="col-8"><input type="number" class="form-control" id="Q" name="Q" value="1" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="y">y</label></div>
            <div class="col-8"><input type="number" class="form-control" id="y" name="y" value="0.1" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="l">l</label></div>
            <div class="col-8"><input type="number" class="form-control" id="l" name="l" value="10" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="X0">X0</label></div>
            <div class="col-8"><input type="number" class="form-control" id="X0" name="X0" value="2.82" required step=".00000000001"></div>
        </div>

        <button type="submit" class="btn btn-primary">Виконати розрахунок</button>
    </form>
@endsection