@extends('layout.app')

@section('content')
    <h3>Математичне та комп’ютерне моделювання одновимірної задачі
        тепло-масопереносу  в ґрунтових масивах при фільтрації підземних вод
    </h3>
    <form action="{{route('lab2.calc')}}" method="post">
        @csrf
        <div class="form-group row">
            <div class="col-2"><label for="N">Варіант N</label></div>
            <div class="col-8"><input type="number" class="form-control" id="N" name="N" value="6" required></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="H1">H1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H1" name="H1" value="1.5" required step=".00000000001"></div>
        </div>

        <div class="form-group  row">
            <div class="col-2"><label for="H2">H2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H2" name="H2" value="0.5" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="l">l</label></div>
            <div class="col-8"><input type="number" class="form-control" id="l" name="l" value="100" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="D">D</label></div>
            <div class="col-8"><input type="number" class="form-control" id="D" name="D" value="0.2" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="h">h</label></div>
            <div class="col-8"><input type="number" class="form-control" id="h" name="h" value="20" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="k">k</label></div>
            <div class="col-8"><input type="number" class="form-control" id="k" name="k" value="1.5" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="sigma">Sigma</label></div>
            <div class="col-8"><input type="number" class="form-control" id="sigma" name="sigma" value="0.2" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="tao">tao</label></div>
            <div class="col-8"><input type="number" class="form-control" id="tao" name="tao" value="30" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="lambda">lambda</label></div>
            <div class="col-8"><input type="number" class="form-control" id="lambda" name="lambda" value="0.14" required step=".00000000001"></div>
        </div>

        <button type="submit" class="btn btn-primary">Виконати розрахунок</button>
    </form>
@endsection