@extends('layout.app')

@section('content')
    <div class="container">
    <div class="row">
    <div class="col-12">
    <h3>Математичне та комп’ютерне моделювання одновимірної задачі
        тепло-масопереносу  в ґрунтових масивах при фільтрації підземних вод</h3>
    <div class="col-12">
        <div >
            {!! $chartjs->render() !!}
        </div>
    </div>
    <div class="text-center">
        <div class="col-12 ">
            <h3>Розподіл концентрацій</h3>
            <table class="table table-dark table-responsive ">
                </thead>
                <tbody>
                @foreach($result as $key=>$item)
                    <tr>
                        <td><strong>Шар - {{$key.' '}} </strong></td>
                        @foreach($item as $item1)
                            <td>{{number_format($item1,12)}}</td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-12">
        <h3>Розподіл температур</h3>

        <div >
            {!! $chartjs1->render() !!}
        </div>
    </div>

    <div class=" text-center p-15">
        <div class="col-12 ">
            <h3>Розподіл темпертур</h3>
            <table class="table table-dark table-responsive ">
                </thead>
                <tbody>
                @foreach($result_temp as $key=>$item)
                    <tr>
                        <td><strong>Шар - {{$key.' '}} </strong></td>
                        @foreach($item as $item1)
                            <td>{{number_format($item1,6)}}</td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    </div>
    </div>
    </div>
@endsection