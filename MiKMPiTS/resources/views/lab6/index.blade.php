@extends('layout.app')

@section('content')
    <h3>МАТЕМАТИЧНЕ МОДЕЛЮВАННЯ МАСОПЕРЕНОСУ З ВРАХУВАННЯМ
        ОСМОСУ НА ВОЛОГОПЕРЕНОС В ОДНОВИМІРНОМУ ВИПАДКУ
    </h3>
    <form action="{{route('lab6.calc')}}" method="post">
        @csrf
        <div class="form-group row">
            <div class="col-2"><label for="H1">H1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H1" name="H1" value="7" required step=".00000000001"></div>
        </div>

        <div class="form-group  row">
            <div class="col-2"><label for="H2">H2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H2" name="H2" value="20" required step=".00000000001"></div>
        </div>
        <div class="form-group  row">
            <div class="col-2"><label for="C1">C1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="C1" name="C1" value="0" required step=".00000000001"></div>
        </div>
        <div class="form-group  row">
            <div class="col-2"><label for="C2">C2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="C2" name="C2" value="10" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="D">D</label></div>
            <div class="col-8"><input type="number" class="form-control" id="D" name="D" value="0.02" required step=".00000000001"></div>
        </div>


        <div class="form-group row">
            <div class="col-2"><label for="T">T</label></div>
            <div class="col-8"><input type="number" class="form-control" id="T" name="T" value="360" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="l">l</label></div>
            <div class="col-8"><input type="number" class="form-control" id="l" name="l" value="15" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="y">y</label></div>
            <div class="col-8"><input type="number" class="form-control" id="y" name="y" value="0.0065" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="lamda">lamda</label></div>
            <div class="col-8"><input type="number" class="form-control" id="lamda" name="lamda" value="0.1" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="p">p</label></div>
            <div class="col-8"><input type="number" class="form-control" id="p" name="p" value="1000" required step=".00000000001"></div>
        </div>

        <div class="form-group  row">
            <div class="col-2"><label for="C">C</label></div>
            <div class="col-8"><input type="number" class="form-control" id="C" name="C" value="350" required step=".00000000001"></div>
        </div>

        <div class="form-group  row">
            <div class="col-2"><label for="C0">C0</label></div>
            <div class="col-8"><input type="number" class="form-control" id="C0" name="C0" value="0" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="sigma">Sigma</label></div>
            <div class="col-8"><input type="number" class="form-control" id="sigma" name="sigma" value="0.5" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="v">v</label></div>
            <div class="col-8"><input type="number" class="form-control" id="v" name="v" value="0.000028" required step=".00000000001"></div>
        </div>

        <button type="submit" class="btn btn-primary">Виконати розрахунок</button>
    </form>
@endsection