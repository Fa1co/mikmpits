@extends('layout.app')

@section('content')
    <h3>Математичне і комп’ютерне моделювання процесу
        фільтраційної консолідації в грунтових масивах
    </h3>
    <form action="{{route('lab5.calc')}}" method="post">
        @csrf
        <div class="form-group row">
            <div class="col-2"><label for="H1">H1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H1" name="H1" value="59" required step=".00000000001"></div>
        </div>

        <div class="form-group  row">
            <div class="col-2"><label for="H2">H2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H2" name="H2" value="26" required step=".00000000001"></div>
        </div>
        <div class="form-group  row">
            <div class="col-2"><label for="C1">C1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="C1" name="C1" value="350" required step=".00000000001"></div>
        </div>
        <div class="form-group  row">
            <div class="col-2"><label for="C2">C2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="C2" name="C2" value="8" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="D">D</label></div>
            <div class="col-8"><input type="number" class="form-control" id="D" name="D" value="0.02" required step=".00000000001"></div>
        </div>


        <div class="form-group row">
            <div class="col-2"><label for="k">k</label></div>
            <div class="col-8"><input type="number" class="form-control" id="k" name="k" value="0.001" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="v">v</label></div>
            <div class="col-8"><input type="number" class="form-control" id="v" name="v" value="0.0028" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="a">a</label></div>
            <div class="col-8"><input type="number" class="form-control" id="a" name="a" value="0.00000512" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="y">y</label></div>
            <div class="col-8"><input type="number" class="form-control" id="y" name="y" value="0.0065" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="e">e</label></div>
            <div class="col-8"><input type="number" class="form-control" id="e" name="e" value="0.6" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="l">l</label></div>
            <div class="col-8"><input type="number" class="form-control" id="l" name="l" value="10" required step=".00000000001"></div>
        </div>


        <div class="form-group row">
            <div class="col-2"><label for="tao">tao</label></div>
            <div class="col-8"><input type="number" class="form-control" id="tao" name="tao" value="30" required step=".00000000001"></div>
        </div>
        <div class="form-group  row">
            <div class="col-2"><label for="C">C</label></div>
            <div class="col-8"><input type="number" class="form-control" id="C" name="C" value="35" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="sigma">Sigma</label></div>
            <div class="col-8"><input type="number" class="form-control" id="sigma" name="sigma" value="0.4" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="h1">h1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="h1" name="h1" value="10" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="Ygr">Ygr</label></div>
            <div class="col-8"><input type="number" class="form-control" id="Ygr" name="Ygr" value="1000" required step=".00000000001"></div>
        </div>

        <button type="submit" class="btn btn-primary">Виконати розрахунок</button>
    </form>
@endsection