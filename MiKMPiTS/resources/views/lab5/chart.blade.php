@extends('layout.app')

@section('content')
    <div class="container">
    <div class="row">
    <div class="col-12">
    <h3>Математичне і комп’ютерне моделювання процесу
        фільтраційної консолідації в грунтових масивах</h3>
    <div class="col-12">
        <div >
            {!! $chartjs->render() !!}
        </div>
    </div>
    <div class="text-center">
        <div class="col-12 ">
            <h3>Задача масопереносу</h3>
            <table class="table table-dark table-responsive ">
                </thead>
                <tbody>
                @foreach($mass as $key=>$item)
                    <tr>
                        <td><strong>Шар - {{$key.' '}} </strong></td>
                        @foreach($item as $item1)
                            <td>{{number_format($item1,3)}}</td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-12">
        <h3>Задача надлишкового напору</h3>

        <div >
            {!! $chartjs1->render() !!}
        </div>
    </div>

    <div class=" text-center p-15">
        <div class="col-12 ">
            <table class="table table-dark table-responsive ">
                </thead>
                <tbody>
                @foreach($napir as $key=>$item)
                    <tr>
                        <td><strong>Шар - {{$key.' '}} </strong></td>
                        @foreach($item as $item1)
                            <td>{{number_format($item1,3)}}</td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    </div>
    </div>
    </div>
@endsection