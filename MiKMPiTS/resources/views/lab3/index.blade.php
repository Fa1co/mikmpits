@extends('layout.app')

@section('content')
    <h3>Математичне та комп’ютерне моделювання двовимірної
        задачі масопереносу розчинених речовин у фільтраційному
        потоці підземних вод</h3>
    <form action="{{route('lab3.calc')}}" method="post">
        @csrf
        <div class="form-group row">
            <div class="col-2"><label for="N">Варіант N</label></div>
            <div class="col-8"><input type="number" class="form-control" id="N" name="N" value="6" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="D">D</label></div>
            <div class="col-8"><input type="number" class="form-control" id="D" name="D" value="0.02" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="h1">h1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="h1" name="h1" value="10" required step=".00000000001"></div>
        </div>

        <div class="form-group  row">
            <div class="col-2"><label for="h2">h2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="h2" name="h2" value="2" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="C">C*</label></div>
            <div class="col-8"><input type="number" class="form-control" id="C" name="C" value="0.1" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="sigma">sigma</label></div>
            <div class="col-8"><input type="number" class="form-control" id="sigma" name="sigma" value="0.4" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="DT">DT</label></div>
            <div class="col-8"><input type="number" class="form-control" id="DT" name="DT" value="0.001" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="tau">tau</label></div>
            <div class="col-8"><input type="number" class="form-control" id="tau" name="tau" value="30" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="y">y</label></div>
            <div class="col-8"><input type="number" class="form-control" id="y" name="y" value="0.0065" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="l">l</label></div>
            <div class="col-8"><input type="number" class="form-control" id="l" name="l" value="10" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="b">b</label></div>
            <div class="col-8"><input type="number" class="form-control" id="b" name="b" value="10" required step=".00000000001"></div>
        </div>

        <button type="submit" class="btn btn-primary">Виконати розрахунок розподілу концентрації мігруючих речовин</button>
    </form>
@endsection