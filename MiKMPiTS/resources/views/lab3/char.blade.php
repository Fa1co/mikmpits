@extends('layout.app')

@section('content')

    <h3>Математичне та комп’ютерне моделювання двовимірної
        задачі масопереносу розчинених речовин у фільтраційному
        потоці підземних вод</h3>
    <div style="width:100%;">
        <div id="chart" style="width:1200px;height:700px;"></div>
        <script>
{{--            var new_data1 = {{json_encode(array_values($result[1]))}};--}}
            var new_data1 = {{json_encode([])}};
            var new_data2 = {{json_encode(array_values($result[2]))}};
            var new_data3 = {{json_encode(array_values($result[3]))}};
            var new_data4 = {{json_encode(array_values($result[4]))}};
            var data_z1 = {z: new_data1, type: 'surface'};
            var data_z2 = {z: new_data2, showscale: false, opacity:0.9, type: 'surface'};
            var data_z3 = {z: new_data3, showscale: false, opacity:0.9, type: 'surface'};
            var data_z4 = {z: new_data4, showscale: false, opacity:0.9, type: 'surface'};
            // console.log(new_data);
            Plotly.newPlot('chart', [data_z1, data_z2, data_z3,data_z4]);
        </script>
    </div>
    <h3>Вихідні дані</h3>
    <div class="row w-100 text-center">
        <div class="col-12 ">
            <h3>Розподіл концентрацій</h3>
            @foreach($result as $key=>$item1)
                <h4>Шар - {{$key.' '}} </h4>
                <table class="table table-dark">
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th><!-- Empty for the left top corner of the table --></th>--}}
                        {{--@foreach($result as $key=>$item)--}}
                            {{--<td><strong>Точка - {{$key.' '}} </strong></td>--}}
                        {{--@endforeach--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    <tbody>
                    @foreach($item1 as $key=>$item)
                        <tr>
                            @foreach($item as $item1)
                                <td>{{number_format($item1,3)}}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endforeach
        </div>
    </div>
@endsection