@extends('layout.app')

@section('content')
    <h3>Математичне та комп’ютерне моделювання одновимірної
        задачі масопереносу розчинених речовин у фільтраційному
        потоці підземних вод</h3>
    <form action="{{route('lab1.calc')}}" method="post">
        @csrf
        <div class="form-group row">
            <div class="col-2"><label for="N">Варіант N</label></div>
            <div class="col-8"><input type="number" class="form-control" id="N" name="N" value="6" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="v1">v1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="v1" name="v1" value="0" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="a1">a1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="a1" name="a1" value="0" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
           <div class="col-2"><label for="H1">H1</label></div>
           <div class="col-8"><input type="number" class="form-control" id="H1" name="H1" value="1.472058" required step=".00000000001"></div>
        </div>

        <div class="form-group  row">
            <div class="col-2"><label for="H2">H2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="H2" name="H2" value="0.596017" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="l">l</label></div>
            <div class="col-8"><input type="number" class="form-control" id="l" name="l" value="500" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="D">D</label></div>
            <div class="col-8"><input type="number" class="form-control" id="D" name="D" value="0.06" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="h">h</label></div>
            <div class="col-8"><input type="number" class="form-control" id="h" name="h" value="20" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="sigma">Sigma</label></div>
            <div class="col-8"><input type="number" class="form-control" id="sigma" name="sigma" value="0.2" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="t1">t1</label></div>
            <div class="col-8"><input type="number" class="form-control" id="t1" name="t1" value="30" required step=".00000000001"></div>
        </div>

        <div class="form-group row">
            <div class="col-2"><label for="t2">t2</label></div>
            <div class="col-8"><input type="number" class="form-control" id="t2" name="t2" value="60" required step=".00000000001"></div>
        </div>
        <div class="form-group row">
            <div class="col-2"><label for="t3">t3</label></div>
            <div class="col-8"><input type="number" class="form-control" id="t3" name="t3" value="90" required step=".00000000001"></div>
        </div>

        <button type="submit" class="btn btn-primary">Виконати розрахунок розподілу концентрації мігруючих речовин</button>
    </form>
@endsection