@extends('layout.app')

@section('content')
<h3>Математичне та комп’ютерне моделювання одновимірної
    задачі масопереносу розчинених речовин у фільтраційному
    потоці підземних вод</h3>
<div style="width:100%;">
    {!! $chartjs->render() !!}
</div>
<h3>Вихідні дані</h3>
<div class="row w-100 text-center">
    <div class="col-12 ">
        <h3>Розподіл концентрацій</h3>
        <table class="table table-dark">
            {{--<thead>
            <tr>
                <th><!-- Empty for the left top corner of the table --></th>
                @foreach($result as $key=>$item)
                    <td><strong>Точка - {{$key.' '}} </strong></td>
                @endforeach
            </tr>--}}
            </thead>
            <tbody>
            @foreach($result as $key=>$item)
                <tr>
                    <td><strong>Шар - {{$key.' '}} </strong></td>
                    @foreach($item as $item1)
                        <td>{{number_format($item1,3)}}</td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection