<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routeslab1.calc
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/lab1','LabController@lab1Index')->name('lab1.index');
Route::post('/lab1','LabController@lab1Calc')->name('lab1.calc');

Route::get('/lab2','LabController@lab2Index')->name('lab2.index');
Route::post('/lab2','LabController@lab2Calc')->name('lab2.calc');

Route::get('/lab3','LabController@lab3Index')->name('lab3.index');
Route::post('/lab3','LabController@lab3Calc')->name('lab3.calc');

Route::get('/lab4','LabController@lab4Index')->name('lab4.index');
Route::post('/lab4','LabController@lab4Calc')->name('lab4.calc');
Route::get('/lab5','LabController@lab5Index')->name('lab5.index');
Route::post('/lab5','LabController@lab5Calc')->name('lab5.calc');

Route::get('/lab6','LabController@lab6Index')->name('lab6.index');
Route::post('/lab6','LabController@lab6Calc')->name('lab6.calc');

Route::get('/lab7','LabController@lab7Index')->name('lab7.index');
Route::post('/lab7','LabController@lab7Calc')->name('lab7.calc');

Route::get('/lab8','LabController@lab8Index')->name('lab8.index');
Route::post('/lab8','LabController@lab8Calc')->name('lab8.calc');

Route::get('/lab9','LabController@lab9Index')->name('lab9.index');
Route::post('/lab9','LabController@lab9Calc')->name('lab9.calc');

Route::get('/test', 'Controller@index');